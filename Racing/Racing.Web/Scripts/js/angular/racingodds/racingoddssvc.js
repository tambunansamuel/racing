﻿(function () {
    "use strict";

    angular.module('racingapp')
        .factory('racingoddssvc', ['$http', racingoddssvc]);

    function racingoddssvc($http) {
        var factory = {
            RetrieveRaceOdds: RetrieveRaceOdds,
            AddHorseBet: AddHorseBet
        };

        return factory;

        /// Implementation

        function RetrieveRaceOdds() {
            return $http.get('/api/RacesAPI/RetrieveHorseOdds');
        }

        function AddHorseBet(horseId, amount) {
            return $http({
                url: '/api/RacesAPI/UpdateHorseOdds',
                method: 'POST',
                data: {
                    HorseId: horseId,
                    Amount: amount
                }
            });
        }
    }
})();