﻿(function () {
    "use strict";

    angular.module('racingapp')
        .controller('racingoddsctrl', ['racingoddssvc', racingoddsctrl]);

    function racingoddsctrl(racingoddssvc) {
        var vm = this;

        vm.Test = "hoho";

        ///// Methods /////
        vm.RetrieveHorseOdds = RetrieveHorseOdds;
        vm.BetAmount = BetAmount;

        /// Values
        vm.RaceOdds = null;

        RetrieveHorseOdds();

        function RetrieveHorseOdds() {
            racingoddssvc.RetrieveRaceOdds()
            .then(function (result) {
                vm.RaceOdds = result.data;
                console.log(vm.RaceOdds);
            }, function (error) {
                alert('There has been an error retrieving the race odds. ' + error.data.ExceptionMessage);
            });
        }

        function BetAmount(horseId, amount) {
            racingoddssvc.AddHorseBet(horseId, amount)
            .then(function (result) {
                // Updates the data
                RetrieveHorseOdds();
            }, function (error) {
                alert('There has been an error adding your bet. ' + error.data.ExceptionMessage);
            }).finally(function () {
                vm.HorseId = null;
                vm.AmountToBet = null;
            });
        }

    }
})();