﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Racing.Core.Interfaces;
using Racing.Core.Models;
using Racing.Infra.Repository;
using Racing.Web.ViewModels;

namespace Racing.Web.Controllers.API
{
    public class RacesAPIController : ApiController {
        #region Private Variables
        IRepo _repo;
        #endregion

        #region Constructor
        public RacesAPIController() {
            this._repo = new MainRepo();
        }
        #endregion

        [HttpGet]
        [Route("api/RacesAPI/RetrieveHorseOdds")]
        public IEnumerable<HorseOdds> RetrieveHorseOdds() {
            return this._repo.RetrieveHorseOdds();
        }

        [HttpPost]
        [Route("api/RacesAPI/UpdateHorseOdds")]
        public void UpdateHorseOdds([FromBody] HorseBetVM horseBet) {
            if (ModelState.IsValid) {
                this._repo.AddHorseBet(horseBet.HorseId, horseBet.Amount);
            } else {
                throw new Exception("Please enter the fields properly.");
            }
        }

    }
}
