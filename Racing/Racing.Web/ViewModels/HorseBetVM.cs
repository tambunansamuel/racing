﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Racing.Web.ViewModels {
    public class HorseBetVM {
        [Required]
        public int HorseId { get; set; }

        [Required]
        public int Amount { get; set; }
    }
}