﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Racing.Core.Interfaces;
using Racing.Core.Models;
using Racing.Infra.Repository;
using System.Linq;

namespace Racing.Test {
    [TestClass]
    public class MainRepoUnitTest {
        #region Private Variables
        IRepo _repo;
        #endregion

        #region Constructor
        public MainRepoUnitTest() {
            this._repo = new MainRepo();
        }
        #endregion

        [TestMethod]
        public void CanRetrieveHorses() {
            var horsesOddsCollection = this._repo.RetrieveHorseOdds();

            Assert.IsNotNull(horsesOddsCollection);
            Assert.IsTrue(horsesOddsCollection.Count() > 0);
        }

        [TestMethod]
        public void CanAddBetToHorses() {
            var horseOddsList = this._repo.RetrieveHorseOdds().ToList();

            this.TotalAmountAddedProperly(2, 10);
            this.TotalAmountAddedProperly(3, 5);
            this.TotalAmountAddedProperly(2, 10);
            this.TotalAmountAddedProperly(2, 10);
            this.TotalAmountAddedProperly(1, 7);
            this.TotalAmountAddedProperly(5, 51);
            this.TotalAmountAddedProperly(4, 34);
            this.TotalAmountAddedProperly(2, 10);
        }

        [TestMethod]
        public void CanRetrieveIndividualHorses() {
            Assert.IsNotNull(this._repo.GetHorseOdds(1));
            Assert.IsNotNull(this._repo.GetHorseOdds(2));
            Assert.IsNotNull(this._repo.GetHorseOdds(3));
            Assert.IsNotNull(this._repo.GetHorseOdds(4));
            Assert.IsNotNull(this._repo.GetHorseOdds(5));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Invalid horse ID")]
        public void InvalidHorseExcptionTest() {
            this._repo.GetHorseOdds(15);
        }

        #region Private Methods
        private void TotalAmountAddedProperly(int horseId, int amount) {
            var currentAmount = this._repo.GetHorseOdds(horseId).TotalAmount;
            var currentOdds = this._repo.GetHorseOdds(horseId).Odds;
            var currentTotalSum = this._repo.RetrieveHorseOdds().Sum(x => x.TotalAmount);

            this._repo.AddHorseBet(horseId, amount);

            var newHorseOdds = this._repo.GetHorseOdds(horseId);

            Assert.AreEqual(currentAmount + amount, newHorseOdds.TotalAmount); // check to ensure the amounts are the same

            Assert.AreEqual(1 - ((decimal)(currentAmount + amount) / (currentTotalSum + amount)), newHorseOdds.Odds); // ensures that the odds are the same
        }
        #endregion
    }
}
