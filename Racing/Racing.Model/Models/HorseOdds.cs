﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racing.Core.Models {
    public class HorseOdds {
        public int Number { get; set; }
        public string HorseName { get; set; }
        public int TotalAmount { get; set; }
        public decimal Odds { get; set; }
    }
}
