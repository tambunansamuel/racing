﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Racing.Core.Models;

namespace Racing.Core.Interfaces {
    public interface IRepo {
        /// <summary>
        /// Retrieves the horse odds
        /// </summary>
        /// <returns>A collection of <see cref="HorseOdds"/> </returns>
        IEnumerable<HorseOdds> RetrieveHorseOdds();

        /// <summary>
        /// Adds the bet to the hrose given the horse ID and the amount.
        /// </summary>
        /// <param name="horseId">ID of the horse</param>
        /// <param name="amount">Amount to be added</param>
        void AddHorseBet(int horseId, int amount);

        /// <summary>
        /// Gets thehorse odds depending on the horse id
        /// </summary>
        /// <param name="horseId">Horse ID to be provided</param>
        /// <returns></returns>
        HorseOdds GetHorseOdds(int horseId);
    }
}
