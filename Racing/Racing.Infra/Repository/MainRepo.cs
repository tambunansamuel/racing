﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Racing.Core.Interfaces;
using Racing.Core.Models;

namespace Racing.Infra.Repository {
    public class MainRepo : IRepo {
        #region Seeded Mock Values
        private static List<HorseOdds> HorsesCollection;
        #endregion

        #region Constructor
        public MainRepo() {
            if (MainRepo.HorsesCollection == null) {
                this.SeedHorseCollection();
                this.UpdateOdds();
            }
        }

        #endregion


        public void AddHorseBet(int horseId, int amount) {
            HorseOdds horseToBet = MainRepo.HorsesCollection.Where(x => x.Number == horseId).FirstOrDefault();

            if (horseToBet == null) { throw new ArgumentOutOfRangeException("horseId", "The horse id is invalid."); }

            horseToBet.TotalAmount += amount;

            this.UpdateOdds();
        }

        public HorseOdds GetHorseOdds(int horseId) {
            var horse = MainRepo.HorsesCollection.Where(x => x.Number == horseId).FirstOrDefault();

            if (horse == null) { throw new ArgumentOutOfRangeException("horseId", "The horse id is invalid."); }

            return horse;
        }

        public IEnumerable<HorseOdds> RetrieveHorseOdds() {
            return MainRepo.HorsesCollection;
        }

        #region Private Methods

        private void SeedHorseCollection() {
            MainRepo.HorsesCollection = new List<HorseOdds>() {
                new HorseOdds() { Number = 1, HorseName ="Black Caviar", TotalAmount = 100},
                new HorseOdds() { Number = 2, HorseName ="Green Moon", TotalAmount = 90},
                new HorseOdds() { Number = 3, HorseName ="Shocking", TotalAmount = 80 },
                new HorseOdds() { Number = 4, HorseName ="Delta Blues", TotalAmount = 70},
                new HorseOdds() { Number = 5, HorseName ="Makybe Diva", TotalAmount = 60},
                new HorseOdds() { Number = 6, HorseName ="Protectionist", TotalAmount = 50},
                new HorseOdds() { Number = 7, HorseName ="Old Rowley", TotalAmount = 40},
                new HorseOdds() { Number = 8, HorseName ="Skipton", TotalAmount = 30}
            };
        }

        private void UpdateOdds() {
            decimal SumTotalAmount = MainRepo.HorsesCollection.Sum(x => x.TotalAmount);

            foreach(var horse in MainRepo.HorsesCollection) {
                horse.Odds = 1 - (horse.TotalAmount / SumTotalAmount);
            }
        }
        #endregion
    }
}
